package Section5Exception;

public class Main {
	public static void main(String[] args){
		new Main();
	}
	public Main(){
		System.out.println("---Tese put, remove and toString---");
		case1();
		System.out.println("---Tese error exception---");
		case2();
	}
		
		
	public void case1(){
			Refrigerator ref1 = new Refrigerator();
			try {
				ref1.put("Apple");
				System.out.println("-Put apple to Refrigerator");
				ref1.put("Banana");
				System.out.println("-Put banana to Refrigerator");
				System.out.println("List of items in Refrigerator."+ref1);
				ref1.takeOut("Apple");
				System.out.println("-takeOut apple from Refrigerator");
				System.out.println("List of items in Refrigerator."+ref1);
			} catch (FullException e) {
				// TODO Auto-generated catch block
				System.err.println("Error: "+e.getMessage());
			}
	}

	public void case2(){
		Refrigerator ref2 = new Refrigerator();
		try{
			ref2.put("Apple");
			System.out.println("-Put apple to Refrigerator");
			ref2.put("Banana");
			System.out.println("-Put banana to Refrigerator");
			ref2.put("Orange");
			System.out.println("-Put orange to Refrigerator");
			ref2.put("Mango");
			System.out.println("-Put mango to Refrigerator");
		}
		catch(FullException e){
			System.err.println("Error: "+e.getMessage());
		}
	}
		

}
