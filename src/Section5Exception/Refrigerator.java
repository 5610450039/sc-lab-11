package Section5Exception;

import java.util.ArrayList;

public class Refrigerator {
	int size = 3;
	ArrayList<String> listRef = new ArrayList<String>();
	
	public void put(String stuff) throws FullException{
		if(listRef.size() >= size){
			throw new FullException("Refrigerator is full, exceeds to put.");
		}
		listRef.add(stuff);
	}
	
	public String takeOut(String stuff) {
		if(listRef.contains(stuff)){
			listRef.remove(stuff);
			return stuff;
		}
		return null;
	}
	
	public String toString(){
		String mes = "";
		for(String s : listRef){
			mes = mes+"\n"+s;
		}
		return mes;
	}
	

}
