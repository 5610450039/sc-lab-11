package Section3HashMap;

import java.util.HashMap;

public class WordCounter {
	
	private String mes;
	HashMap<String,Integer> wordCount = new HashMap();
	
	public WordCounter(String ames){
		mes = ames;
		
	}
	
	public void count(){
		String [] sm = mes.split(" ");
		for(String i : sm){
			if(wordCount.containsKey(i)){
				wordCount.put(i,wordCount.get(i)+1);
				
			}
			else{
				wordCount.put(i,1);
			}
		}
		
	}
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		return 0;
	}
}
